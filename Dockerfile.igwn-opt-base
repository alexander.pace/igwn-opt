FROM igwn/builder:el8-testing

LABEL name "igwn-opt base container" \
      maintainer="Alexander Pace <alexander.pace@ligo.org>" \
      date="2022-06-28" \
      support="Rocky Linux 8.5"

# Collect buildargs from the command line
ARG support_packages
ARG blas_lib

USER root

# Get list of support packages from environment and install.
RUN yum -y install $support_packages && \
    yum clean all

# Install Intel MKL repo. This repo will be available regardless
# of architecture, but libraries will only install for x86_64.
# Also install lscsoft-production (source) for RL and SL7:
COPY misc/oneAPI.repo /etc/yum.repos.d/
COPY misc/lscsoft-src.repo /etc/yum.repos.d/

# Install BLAS libraries. 
RUN yum -y install intel-oneapi-mkl-2021.4.0 atlas-devel openblas-devel && \
    yum clean all

# Now disable the intel oneapi repo. the GPG verification has been 
# problematic and it's not needed at this point:
RUN yum-config-manager --disable oneAPI

# Intel MKL and gcc-devtoolset variables go here:
ENV_LINE_ENV_LINE_ENV_LINE

# Expand BLAS_LIBS varibles:
ENV BLAS_LIB $blas_lib
